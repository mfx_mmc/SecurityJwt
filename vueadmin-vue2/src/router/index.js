import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import axios from "axios";
import store from '../store'
import locale from "element-ui/lib/locale/lang/zh-CN";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/login',
    children: [
      {
        path: '/index',
        name: '/index',
        component: () => import('../views/Index')
      },
      // {
      //   path: '/sys/user',
      //   name: 'User',
      //   component: () => import('../views/User')
      // },
      // {
      //   path: '/sys/role',
      //   name: 'Role',
      //   component: () => import('../views/Role')
      // },
      // {
      //   path: '/sys/menu',
      //   name: 'Menu',
      //   component: () => import('../views/Menu')
      // },
      {
        path: '/sys/person',
        name: 'Person',
        component: () => import('../views/Person')
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: ()=>import('../views/Login')
  }
]

const router = new VueRouter({
  routes
})
//vue-router全局导航守卫
router.beforeEach((to, from, next) => {

  //获取路由状态
  let hasRoute = store.state.menus.hasRole;
  //当hasRoute为false时，才去进行动态路由绑定，
  //若为true则认为已经绑定过，则不需要再次绑定
  if(!hasRoute){
    //此处使用的axios不是我们封装好的axios.js,
    //因为我们封装好的axios里面会在请求中添加headers所以我们在这里也要加上header
    axios.get('/sys/menu/nav',{
      headers: {
        Authorization: localStorage.getItem("token"),
      }
    }).then(res => {
      //一、动态导航
      //拿到menuList：对应用户菜单权限
      let menuList = res.data.data.nav
      let permList = res.data.data.authorities
      store.commit("setMenuList",menuList)
      //拿到用户权限：用户是否能执行某些操作
      store.commit("setPermList",permList)
      //二、动态路由绑定
      //1、获取当前路由表
      let newRoutes = router.options.routes;

      //2、遍历后端返回的菜单项，将子菜单项添加到路由表中
      res.data.data.nav.forEach(menu => {
        //2.1如果有子菜单
        if(menu.children){
          //遍历子菜单
          menu.children.forEach(item => {
            //自定义menuToRoute方法将子菜单项转为路由项
            let route = menuToRoute(item)
            //当route不为空时添加到newRoutes
            if(route){
              newRoutes[0].children.push(route)
            }
          })
        }
      })
      //3、添加路由
      // console.log("newRoutes")
      // console.log(newRoutes)
      router.addRoutes(newRoutes)
      //将hashRole值设为true，下次可以不用再进行路由绑定
      store.commit("changeRouteState",true)
    })

  }

  next()
})

//导航转路由方法
const menuToRoute = (menu) =>  {
 // 只有当component不为空时才转为路由
 if(menu.component){
   return{
     path: menu.path,
     name: menu.name,
     //meta中可以放一些额外之，权限就可以放在里面
     meta: {
       icon: menu.icon,
       title: menu.title
     },
     //
     component: () => import('../views/'+menu.component)
   }
 }
 return null;
}
export default router
