import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Element from 'element-ui'
//国际化
import locale from 'element-ui/lib/locale/lang/zh-CN'
import 'element-ui/lib/theme-chalk/index.css'
import axios from './axios'
import global from './globalFun'
//设置全局变量，使用axios
Vue.prototype.$axios = axios

Vue.config.productionTip = false
Vue.use(Element,{locale})

//引用mock.js,模拟生成前端假数据
// require('./mock.js')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
