import Vue from 'vue'
import Vuex from 'vuex'
import menus from '../store/modules/menu'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: ''
  },
  //异步操作state中的变量
  mutations: {

    //将token放入store,并且存储再localStorage
    SET_TOKEN: (state,token) => {
      //mutations中函数会有两个参数:1:state(固定参数,指state),2:其他参数(可以是传过来的数据)
      //这里只能写state,不能写this.state,否则会报错
      state.token = token;
      localStorage.setItem("token",token);
    },
    //logout清除用户信息
    resetState:  (state) => {
      state.token = '';
    }
  },
  actions: {
  },
  modules: {
    //引用模块
    menus,
  }
})
