import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
//错误写法export default new Vuex.Store({})
//正确写法export default{} ，因为Store只能有一个，并且放在store下的index.js
//记得在index.js中引用该模块
export default {
    state: {
        menuList: [],
        permList: [],//权限列表
        //因为刷新页面都会执行router.beforeEach函数，但是该函数只需要执行一次就可以了
        // 所以设置hasRole用来判断该函数是否执行过
        hasRole: false,
        //标签页变量
        editableTabsValue: '/index',
        editableTabs: [
            {
                title: '首页',
                name: '/index'
            }
        ],
        tabIndex: 2
    },
    //异步操作state中的变量
    mutations: {
        setMenuList(state,menuList){
            state.menuList = menuList;
        },
        setPermList(state,permList){
            state.permList = permList;
        },
        //修改hasRole值
        changeRouteState(state,hashRole){
            state.hasRole = hashRole;
        },
        //从session中获取editableTabsValue
        getEditableTabsValue(state,editableTabsValue){
            state.editableTabsValue = editableTabsValue
        },
        //从session中获取editableTabs
        getEditableTabs(state,editableTabs){
            state.editableTabs = editableTabs
        },
        //用户退出时，将store中的editableTabs信息设为默认值
        resetEditableTabs(state){
            state.editableTabs = [
                {
                    title: '首页',
                    name: '/index'
                }
            ];
        },
        //用户退出时，将store中的editableTabsValue信息设为默认值
        resetEditableTabsValue(state){
            state.editableTabsValue = '/index';
        },
        //添加标签页
        addTab(state,item){
            //当标签页数组中没有当前标签时才添加。
            let index = state.editableTabs.findIndex(e => e.name===item.name)
            if(index===-1){
                state.editableTabs.push({
                    title: item.title,
                    name: item.name
                })
            }
            //将新添加标签页激活：高亮
            state.editableTabsValue = item.name
        }
     },
    actions: {
    },

}
