const Mock = require('mockjs')

//获取Mock  Random对象
const Random = Mock.Random

let Result = {
    code: 200,
    msg: '操作成功',
    data: null
}

//模拟返回验证码图片
Mock.mock('/captcha','get',()=>{
    //返回数据要加return
    Result.data = {
        token: Random.string(32),
        //注意图片大小中间的乘号,其官网复制
        captchaImg: Random.dataImage('120x40','p7n5w')
    }
    return Result;
})

//模拟返回jwt token
Mock.mock('/login','post',()=>{
    // //mock 无法再header中存入jwt数据,这里只是模拟
    // //模拟400错误,检验axios请求拦截器是否正常
    // Result.code = 400
    // Result.msg = '验证码错误'
    return Result;
})
//模拟logout
Mock.mock('/logout','post',()=>{
    //mock 无法再header中存入jwt数据,这里只是模拟
    //模拟400错误,检验axios请求拦截器是否正常
    Result.code = 200
    return Result;
})
//模拟请求菜单权限和功能权限
Mock.mock('/sys/menu/nav','get',()=>{
    //mock 无法再header中存入jwt数据,这里只是模拟
    //模拟400错误,检验axios请求拦截器是否正常
    let nav = [{
        name: 'SysManga',//对应index
        title: '系统管理',
        icon: 'el-icon-s-operation',
        path: '',//router-link跳转路由
        component: '',
        children:  [
            {
                name: 'SysUser',
                title: '用户管理',
                icon: 'el-icon-s-custom',
                path: '/sys/user',
                component: 'User',
                children: []
            },
            {
                name: 'SysRole',
                title: '角色管理',
                icon: 'el-icon-s-custom',
                path: '/sys/role',
                component: 'Role',
                children: []
            },
            {
                name: 'SysMenu',
                title: '菜单管理',
                icon: 'el-icon-s-custom',
                path: '/sys/menu',
                component: 'Menu',
                children: []
            },

        ]
    },
        {
            name: 'SysTools',
            title: '系统工具',
            icon: 'el-icon-s-tools',
            path: '',
            component: '',
            children: [
                {
                    name: 'SysDict',
                    title: '数字字典',
                    icon: 'el-icon-s-order',
                    path: '/sys/dicts',
                    component: '',
                    children: []
                },
            ]
        },
    ]
    let authorities = ['sys:user:save','sys:user:del']
    Result.data = {
        nav: nav,
        authorities: authorities
    }
    return Result;
})
//模拟返回菜单树
Mock.mock('/sys/menu/list','get',()=>{

    let tableData = [{
            id: 1,
            name: '系统管理',
            statu: 1,
            type: 0,
            children: [{
                id: 11,
                name: '用户管理',
                type: 1,
            }, {
                id: 12,
                name: '角色管理',
                type: 1,

            },{
                id: 13,
                name: '菜单管理',
                type: 1,

            }]
        }, {
            id: 2,
            name: '系统工具',
            statu: 1,
            type: 0,
        }]

        //返回数据要加return
    Result.data = tableData;
    return Result;
})
Mock.mock('/sys/menu/save','post',() => {
    Result.data = "新增"
    return Result;
})
//获取角色信息列表
Mock.mock(RegExp('/sys/role/list*'),'get',() => {
    Result.data = {
        "records": [{
            id: 1,
            name: '普通用户',
            code:  'sys:role:save',
            statu: 1,
            remark: '上海市普陀区金沙江路 1518 弄'
        }, {
            id: 2,
            name: '超级管理员',
            code:  'sys:role:update',
            statu: 0,
            remark: '上海市普陀区金沙江路 1518 弄'
        },
            {
                id: 3,
                name: '普通用户',
                code:  'sys:role:save',
                statu: 1,
                remark: '上海市普陀区金沙江路 1518 弄'
            },
            {
                id: 4,
                name: '普通用户',
                code:  'sys:role:save',
                statu: 1,
                remark: '上海市普陀区金沙江路 1518 弄'
            }, {

                id: 5,
                name: '超级管理员',
                code:  'sys:role:update',
                statu: 0,
                remark: '上海市普陀区金沙江路 1518 弄'
            },
            {
                id: 6,
                name: '普通用户',
                code:  'sys:role:save',
                statu: 1,
                remark: '上海市普陀区金沙江路 1518 弄'
            }],
        "total":  6,
        "size": 10,
        "current": 1,
        "orders": [],
        "optimizeCountSql": true,
        "hitCount": false,
        "countId": null,
        "maxLimit": null,
        "searchCount": true,
        "pages": 1
    }

    return Result
})
//角色信息
Mock.mock(RegExp('/sys/role/info/*'),'get',() => {
    Result.data = {
        "id": 6,
        "statu": 1,
        "name": "超级管理员",
        "code": "admin",
        "remark": "系统默认最高权限",
        "menuIds": [13]
    }
    return Result
})
// 用户信息
Mock.mock(RegExp('/sys/user/list/*'),'get',() => {
   Result.data = {
       "records": [{'id': 1,
           "username":'今天鲍鱼有点咸',
           'avatar':"https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png",
           'roles':[{'name':'普通用户'},{'name':'超级管理员'}],
           "email": '3257676272@qq.com',
           "phone": '123456',
           "statu": 1,
           "created": '2021-11-18'
       }],
       "current":1,
       "size":10,
       "total":100

   }
    return Result
})