import Vue from 'vue'
//创建全局可以调用的函数
Vue.mixin({
    methods:{
        //判断是否拥有某个权限,返回值为true或false
        hasAuth(perm){
            let authority = this.$store.state.menus.permList;
            return authority.indexOf(perm) > -1
        }
    }
})