package com.komorebi.common;

public class Const {
    public final static String CAPTCHA_KEY = "captcha";
    public final static Integer STATUS_ON = 0;
    public final static Integer STATUS_OFF = 1;
}
