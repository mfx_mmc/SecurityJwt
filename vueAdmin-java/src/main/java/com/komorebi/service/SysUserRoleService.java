package com.komorebi.service;

import com.komorebi.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
