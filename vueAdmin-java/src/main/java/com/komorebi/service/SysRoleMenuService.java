package com.komorebi.service;

import com.komorebi.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
