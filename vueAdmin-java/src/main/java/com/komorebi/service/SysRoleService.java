package com.komorebi.service;

import com.komorebi.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysRoleService extends IService<SysRole> {

}
