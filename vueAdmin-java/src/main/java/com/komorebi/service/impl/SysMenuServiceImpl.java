package com.komorebi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.komorebi.entity.SysMenu;
import com.komorebi.entity.SysUser;
import com.komorebi.mapper.SysMenuMapper;
import com.komorebi.mapper.SysUserMapper;
import com.komorebi.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.komorebi.service.SysUserService;
import com.komorebi.vo.SysMenuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    SysUserMapper sysUserMapper;
    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysMenuService sysMenuService;
    @Override
    public List<SysMenuVo> getCurrentUserNav() {
        //获得当前用户名
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SysUser sysUser = sysUserService.getByUserName(username);
        //mapper文件实现通过userid查询导航栏菜单id
        List<Long> navMenuIds = sysUserMapper.getNavMenuIds(sysUser.getId());
        List<SysMenu> sysMenus = this.listByIds(navMenuIds);

        //转树状结构
        List<SysMenu> menuTree = buildTreeMenu(sysMenus);
        //实体转vo
        return copyList(menuTree);
    }

    //获取系统所有的菜单项
    @Override
    public List<SysMenu> tree() {
        //返回的结果按照orderNum升序排序
        List<SysMenu> sysMenus = sysMenuService.list(new QueryWrapper<SysMenu>().orderByAsc("orderNum"));
        List<SysMenu> menuList = buildTreeMenu(sysMenus);
        return menuList;
    }

    private List<SysMenu> buildTreeMenu(List<SysMenu> sysMenus) {
        ArrayList<SysMenu> finalMenus = new ArrayList<>();
        //先寻找各自的孩子
        for(SysMenu menu : sysMenus){
            for(SysMenu item : sysMenus){
                if(menu.getId() == item.getParentId()){
                    menu.getChildren().add(item);
                }
            }
            //提取出父节点
            if(menu.getParentId() == 0){
                finalMenus.add(menu);
            }
        }
        return finalMenus;
    }

    private List<SysMenuVo> copyList(List<SysMenu> menuTree) {
        ArrayList<SysMenuVo> sysMenuVos = new ArrayList<>();
        for(SysMenu menu : menuTree){
            SysMenuVo sysMenuVo = new SysMenuVo();
            sysMenuVo.setId(menu.getId());
            sysMenuVo.setName(menu.getPerms());
            sysMenuVo.setTitle(menu.getName());
            sysMenuVo.setPath(menu.getPath());
            sysMenuVo.setIcon(menu.getIcon());
            sysMenuVo.setComponent(menu.getComponent());
            if(menu.getChildren().size() > 0){
                //递归调用
                sysMenuVo.setChildren(copyList(menu.getChildren()));
            }
            sysMenuVos.add(sysMenuVo);
        }
        return sysMenuVos;
    }
}
