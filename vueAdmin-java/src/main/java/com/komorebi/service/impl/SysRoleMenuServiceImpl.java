package com.komorebi.service.impl;

import com.komorebi.entity.SysRoleMenu;
import com.komorebi.mapper.SysRoleMenuMapper;
import com.komorebi.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
