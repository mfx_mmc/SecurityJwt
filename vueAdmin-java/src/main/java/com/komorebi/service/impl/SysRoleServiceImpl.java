package com.komorebi.service.impl;

import com.komorebi.entity.SysRole;
import com.komorebi.mapper.SysRoleMapper;
import com.komorebi.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
