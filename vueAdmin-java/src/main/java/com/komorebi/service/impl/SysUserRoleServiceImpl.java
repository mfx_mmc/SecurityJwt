package com.komorebi.service.impl;

import com.komorebi.entity.SysUserRole;
import com.komorebi.mapper.SysUserRoleMapper;
import com.komorebi.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
