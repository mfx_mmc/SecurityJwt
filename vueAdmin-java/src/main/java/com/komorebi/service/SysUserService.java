package com.komorebi.service;

import com.komorebi.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysUserService extends IService<SysUser> {

    SysUser getByUserName(String username);

    String getUserAuthority(Long userId);
    //当用户权限改变时清空缓存
    void clearUserAuthorityInfo(String username);
    //当用户的角色信息改变时清空缓存
    void clearUserAuthorityInfoByRoleId(Long roleId);
    //当用户的菜单改变时清空缓存
    void clearUserAuthorityInfoByMenuId(Long menuId);

}
