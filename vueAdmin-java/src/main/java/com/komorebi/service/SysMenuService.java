package com.komorebi.service;

import com.komorebi.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.komorebi.vo.SysMenuVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysMenuService extends IService<SysMenu> {

    List<SysMenuVo> getCurrentUserNav();

    List<SysMenu> tree();
}
