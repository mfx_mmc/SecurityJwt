package com.komorebi.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
/*验证码图片生成配置类
* 设置验证码图片的相关配置信息
* */
public class KaptchaConfig {
    //Kaptch:google验证码工具
    @Bean
    DefaultKaptcha producer(){
        //produce用于生成验证码图片
        Properties properties = new Properties();
        //设置图片是否有边框
        properties.put("kaptcha.border", "no");
        //设置图片字体颜色
        properties.put("kaptcha.textproducer.font.color", "black");
        //设置图片中字符键空格
        properties.put("kaptcha.textproducer.char.space", "4");
        //设置图片高度
        properties.put("kaptcha.image.height", "40");
        //设置图片宽度
        properties.put("kaptcha.image.width", "120");
        //设置图片字体
        properties.put("kaptcha.textproducer.font.size", "30");
        //Kaptcah中的Config
        Config config = new Config(properties);
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
