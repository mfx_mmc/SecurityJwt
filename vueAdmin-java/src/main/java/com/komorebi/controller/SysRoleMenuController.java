package com.komorebi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/sys-role-menu")
public class SysRoleMenuController extends BaseController {

}
