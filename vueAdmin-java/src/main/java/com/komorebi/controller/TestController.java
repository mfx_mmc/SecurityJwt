package com.komorebi.controller;

import com.komorebi.common.Result;
import com.komorebi.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    SysUserService sysUserService;

    @GetMapping
    @PreAuthorize("hasRole('admin')")
    public Object test(){
        return Result.success(sysUserService.list());
    }

}
