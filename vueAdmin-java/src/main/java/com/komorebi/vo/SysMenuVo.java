package com.komorebi.vo;

import com.komorebi.entity.SysMenu;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/*
*               name: 'SysUser',
                title: '用户管理',
                icon: 'el-icon-s-custom',
                path: '/sys/user',
                component: 'User',
                children: []
* */
@Data
public class SysMenuVo implements Serializable {
    private Long id;
    private String  name;
    private String title;
    private String  icon;
    private String path;
    private String component;
    private List<SysMenuVo> children;
}
