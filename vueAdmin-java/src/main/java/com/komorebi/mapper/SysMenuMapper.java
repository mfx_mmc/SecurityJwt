package com.komorebi.mapper;

import com.komorebi.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */

public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
