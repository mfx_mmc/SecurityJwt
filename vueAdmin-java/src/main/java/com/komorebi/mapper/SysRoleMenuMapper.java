package com.komorebi.mapper;

import com.komorebi.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author komorebi
 * @since 2021-11-19
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
