package com.komorebi.security;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.komorebi.entity.SysUser;
import com.komorebi.mapper.SysUserMapper;
import com.komorebi.service.SysUserService;
import com.komorebi.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//jwt认证过滤器
@Slf4j
public class JwtAuthenticationFilter extends BasicAuthenticationFilter {
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    UserDetailServiceImpl userDetailService;
    @Autowired
    SysUserService sysUserService;
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("开启jwt认证");

        String jwt = request.getHeader(jwtUtils.getHeader());
        if(jwt != null){
            Claims claim = jwtUtils.getClaimByToken(jwt);
            if(claim != null){
                String username = claim.getSubject();
                log.info("jwt认证:检查用户名");
                if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                    SysUser sysUser = sysUserService.getByUserName(username);
                    Long userId = sysUser.getId();
                    UserDetails userDetails = userDetailService.loadUserByUsername(username);
                    if(!jwtUtils.isTokenExpired(claim)){
                        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, null, userDetailService.getUserAuthority(userId));
                        auth.setDetails(userDetails);
                        log.info("{}",auth);
                        log.info("通过jwt认证，设置Authentication,后续过滤器放行");
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                }
            }
        }else {
            log.info("首次登陆 jwt为空");
        }
        chain.doFilter(request,response);
        /*

        String jwt = request.getHeader(jwtUtils.getHeader());
        if(StrUtil.isBlankOrUndefined(jwt)){
            //继续下一个过滤器
            chain.doFilter(request,response);
            return;
        }
        Claims claim = jwtUtils.getClaimByToken(jwt);
        if(claim == null){
            throw new JwtException("token异常");
        }
        if(jwtUtils.isTokenExpired(claim)){
            throw new JwtException("token已过期");
        }
        String username = claim.getSubject();
        SysUser sysUser = sysUserService.getByUserName(username);
        Long userId = sysUser.getId();
        //对于使用 token 鉴权的系统，我们就可以验证token后手动填充SecurityContextHolder

        //获取用户的权限等信息
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, userDetailService.getUserAuthority(userId));
        //设置认证主体，如果不设置在UsernamePasswordAuthenticationToken认证时就会失败，登录失败

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request,response);
        */

    }
}
